import express from 'express'
import dotenv from 'dotenv'
import multer from 'multer'
import fetch from 'node-fetch';
import grant from 'grant-express';
import { client } from './db_connect'
import {userController} from './main'
export const loginRoutes = express.Router();

//#region Fields
const SALT_ROUNDS = 10;

//#endregion

//#region Storage
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const upload = multer({ storage })

//#endregion

//#region Google
class GoogleLogin {
    public static getMediator(): express.RequestHandler {
        dotenv.config()
        return grant({
            "defaults": {
                // "protocol": "http",
                // "host": "localhost:8080",
                "origin": "http://localhost:8080",
                "transport": "session",
                "state": true,
            },
            "google": {
                "key": process.env.GOOGLE_CLIENT_ID || "",
                "secret": process.env.GOOGLE_CLIENT_SECRET || "",
                "scope": ["profile", "email"],
                "callback": "/login/google"
            }
        }) as express.RequestHandler;
    }
}

loginRoutes.use(GoogleLogin.getMediator());
// export const google = new GoogleLogin();

loginRoutes.get('/login/google',userController.loginGoogle )

//#endregion

//#region Local Login
export const isLoggedIn = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.session?.["user"] == null) {
        res.status(401).json('唔得')
        return;
    } else {
        let user = req.session["user"]
        console.log(user)
        next();
    }
}

loginRoutes.post('/is_logged_in', upload.none(),userController.checkLogin )
loginRoutes.get('/getUser', upload.none(),userController.getUser)

loginRoutes.post('/login', upload.none(), )
loginRoutes.post('/logout', (req, res) => {
    req.session?.destroy(() => {
        res.redirect('/')
    });
})


// function addDefaultUser() {
//     addUser("zeus@tecky.io", "");
//     // addUser("zeuscsc@gmail.com", "");
//     console.log("User added")
// }
// addDefaultUser();


//#endregion
