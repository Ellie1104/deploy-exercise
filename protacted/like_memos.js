window.onload = async () => {  // should be inside window.onload
    const searchParams = new URLSearchParams(location.search);
    const id = searchParams.get('id');

    // Use the id to fetch data from 
    const res = await (`/like_memos?userid=${id}`);
    const liked_memos = await res.json();
    // Rest of the code 
}