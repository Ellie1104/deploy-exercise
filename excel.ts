import { client } from './db_connect';

import dotenv from 'dotenv';
import xlsx from 'xlsx';

async function main() {
    let workbook = xlsx.readFile('WSP009-exercise.xlsx');
    let sheet_name_list = workbook.SheetNames;
    let users: User[] = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
    let itemsCount = 1;
    let items = [];
    class User {
        username: string;
        password: string;
    }
    for (let user of users) {
        let insert_query =
            "INSERT INTO users (username,password,created_t,updated_at) VALUES ($1,$2,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";
        // client.query(insert_query, [user.username, user.password]);
    }
    class Memo {
        content: string;
    }

    // let memos = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[1]]);
    // console.log(memos);
    // for (let memo of memos) {
    //     let insert_query =
    //         "INSERT INTO memos (content,created_t,updated_at)"
    // }
    // const res = await client.query('SELECT * from users where username = $1', ['gordon'])
    // console.log(res.rows[0].username) // gordon
    // await client.end()
}
main();

// pg_dump - U postgres - W DB_NAME > schema.sql
// psql - U postgres - W DB_NAME < schema.sql