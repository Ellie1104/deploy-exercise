
// const sortable = new Draggable.Sortable(document.querySelectorAll('.postit'), {
//     draggable: '.postit'
// });
document.querySelector("#memo_content").addEventListener("input", function (event) {
    let textarea = document.querySelector("#memoEditDisplayText");
    textarea.textContent = event.target.value;
    // textarea.textContent = textarea.textContent.replace("", "");
    // textarea.innerHTML = textarea.textContent.replace("\n", "<br>");
    // textarea.textContent = textarea.innerHTML;
})
document.querySelector("#add-form").addEventListener("submit", event => {
    event.preventDefault();
    addForm();
})
document.querySelector("#login").addEventListener("submit", event => {
    event.preventDefault();
    login();
})
let loggedIn = false;
async function login() {
    const body = new FormData(document.querySelector("#login"))
    const res = await fetch("/login", {
        method: 'POST',
        body: body
    })
    let json = await res.json();
    if (json.result) {
        loggedIn = json.result;
    }
    readMemos();
}
async function isLoggedIn() {
    const res = await fetch('/getUser')
    const json = await res.json();
    const user = json.user;
    if (user)
        loggedIn = user.login_success
}
async function addForm() {
    const body = new FormData(document.querySelector("#add-form"))
    await fetch("/add-memo", {
        method: 'POST',
        body: body
    })
    readMemos();
}

async function readMemos() {
    const res = await fetch('/memos')
    const json = await res.json();
    console.log(json)
    const postits = document.querySelector("#postits")
    postits.innerHTML = "";
    for (const item of json) {
        let note = item.content;
        if (note.message == null) note.message = "";

        let html = postits.innerHTML;
        html += `<div class="col-sm-3 col-12 postit">`;
        html += `<img class="memo" id="postit" src="postit.png">`;
        if (loggedIn)
            html += `<button class="edit" data-id="${item.id}"><i class="fas fa-edit"></i></button>`;
        html += `<div class="memoCentered">`;
        html += `<h6 id="memoEditDisplayText">${note.message}</h6>`;
        html += `</div>`;
        if (loggedIn)
            html += `<button class="remove" data-id="${item.id}"><i class="fas fa-trash-alt"></i></button>`;
        html += `</div>`;
        postits.innerHTML = html;
    }

    const editButtons = document.querySelectorAll(".postit .edit")
    for (const button of editButtons) {
        button.addEventListener("click", async () => {
            const newContent = prompt("New Content");
            let json = { message: newContent };
            await fetch("/memos/" + button.dataset.id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(json)
            })
            readMemos();
        })
    }
    const removeButtons = document.querySelectorAll(".postit .remove")
    for (const button of removeButtons) {
        button.addEventListener("click", async () => {
            await fetch("/memos/" + button.dataset.id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            readMemos();
        })
    }
}

const socket = io.connect();
socket.on('memo-update', () => {
    readMemos();
})
isLoggedIn();
readMemos();