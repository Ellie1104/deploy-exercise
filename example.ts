// npm install express @types/express
import express from 'express'
import path from 'path'

const app = express();

app.use((req, res, next) => {
    console.log(req.ip)
    console.log(req.path)
    next()
});

app.use(express.static('./html'));

app.post("/post", callback)
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "index.html"))
});
app.get("/");
app.listen(8080);

function callback(formRequest:express.Request, response:express.Response): void {
    response.send();
}