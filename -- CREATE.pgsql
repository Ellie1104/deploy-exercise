-- CREATE
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS memos;
CREATE TABLE users(
id SERIAL primary key,
username VARCHAR(255),
password VARCHAR(255),
ct TIMESTAMP,
ut TIMESTAMP
);
CREATE TABLE memos(
id SERIAL primary key,
content JSONB,
image_path VARCHAR(255),
ct TIMESTAMP,
ut TIMESTAMP
);