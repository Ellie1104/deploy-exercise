import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
   let id = await knex("users").insert([
        { username:"a", password:"abc" },
        { username:"b", password:"abc" },
        { username:"c", password:"abc" },
    ]).returning("id")
    console.log(id)
};
