import {MemoService} from '../services/memoService'
import {MemoController} from './memoController'
import {Request, Response} from 'express';

import SocketIO from 'socket.io';
import Knex from "knex"
jest.mock('express')

describe('Memo Controller',()=>{
    let controller:MemoController;
    let service:MemoService;
    let io: SocketIO.Server;    
    let emit:jest.Mock;
    let req : Request;
    let res : Response;

    beforeEach(()=>{
        service = new MemoService({}as Knex);
        controller = new MemoController(service, io);
        jest.spyOn(service,'getMemos').mockImplementation(()=>Promise.resolve(
            [
                {
                    id:1, content:{"color":"#000000", "message":"abc"}
                }
            ]
        ));
       

        emit = jest.fn((event, msg)=> null);
        io = {
            emit:emit
        }as any as SocketIO.Server;
        
        req = {
            body:{
                content:{"color":"#000000", "message":"abc"}
            },
            params:{
                id:1
            },
            session:{
                user:{
                    id:1
                }
            }
        }as any as Request;   //what does this mean?

        jest.spyOn(service,'addMemo').mockImplementation((req)=>Promise.resolve())  //why cannot pass req.body.content
        res = {
            json: () =>{}
        } as any as Response;

        res.json = jest.fn();

        jest.spyOn(service,'deleteMemo').mockImplementation((id)=>Promise.resolve())
        jest.spyOn(service,'editMemo').mockImplementation((id, content)=>Promise.resolve())
    })

    it("get memos",async()=>{
        await controller.getMemo(req,res)
        expect(service.getMemos).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith([{
            "id":1, "content":{"color":"#000000", "message":"abc"}
        }])
    })

    it("add memo",async()=>{
        await controller.addMemo(req,res)
    
        expect(service.addMemo).toBeCalledTimes(1); //why service represent this.service? is this fake?
        // expect(io.emit).toBeCalledTimes(1);    
        expect(res.json).toBeCalledWith({result:true})
    })
    
    it("edit memo", async()=>{
        await controller.editMemo(req,res)
        expect(service.editMemo).toBeCalledWith(1,{
            content:{"color":"#000000", "message":"abc"}
        })
        expect(res.json).toBeCalledWith({result:true})
    })

    it("delete memo", async()=>{
        await controller.deleteMemo(req,res)
        expect(service.deleteMemo).toBeCalledWith(1)
        // expect(io.emit).toBeCalledTimes(1)
        expect(res.json).toBeCalledWith({result:true})
    })
})