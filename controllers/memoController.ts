import SocketIO from 'socket.io'
import {MemoService} from '../services/memoService'
import {Request, Response} from 'express';
// import multer from 'multer'

export class MemoController{

    constructor(private service:MemoService, private io: SocketIO.Server){
        
    }


    public addMemo = async (req:Request, res:Response) => {
  
      await this.service.addMemo(req.body)
        this.io.emit('memo-update')
        res.json({ result: true })
    }

public getMemo = async (req:Request, res:Response) => {
    const notes = await this.service.getMemos();
    // console.log(notes);
    res.json(notes)
}

public editMemo = async (req:Request, res:Response) => {
    await this.service.editMemo(Number(req.params.id), req.body);
    this.io.emit('memo-update')
    res.json({ result: true })
}

public deleteMemo = async (req:Request, res:Response) => {
     this.service.deleteMemo(Number(req.params.id));
    this.io.emit('memo-update')
    res.json({ result: true })
}
}