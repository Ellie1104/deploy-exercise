// import dotenv from 'dotenv'
// import grant from 'grant-express';
import { Request, Response } from 'express';
import { UserService } from '../services/userService'


export class UserController {

    constructor(private userService: UserService) {

    }

    public loginGoogle = async (req: Request, res: Response) => {
        const accessToken = req.session?.['grant'].response.access_token;
        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        });
        const json = await fetchRes.json();
        // console.log(json)
        if (json.verified_email) {
            let users = (await this.userService.getUsers(json.email))
            if (users.length < 1) {
                this.userService.getUsers(json.email, json.id)
                users = (await this.userService.getUsers(json.email))
            }
            const user = users[0];
            if (req.session) {
                req.session['user'] = {
                    id: user.id,
                    email: json.email,
                    name: json.name,
                    given_name: json.given_name,
                    family_name: json.family_name,
                    login_success: true
                };
            }
        }
        res.redirect('/')
    }


    public checkLogin = async (req:Request, res:Response) => {
        if (req.session?.["user"]) {
            let user = req.session["user"]
            res.json({ result: user.login_success })
        } else {
            res.json({ result: false })
        }
    }

    public getUser = async (req:Request, res:Response) => {
        // console.log(req.session["user"])
        if (req.session?.["user"]) {
            res.json({ user: req.session["user"] })
        } else {
            res.json({ user: null })
        }
    }


    public login = async (req:Request, res:Response) => {
        const users = await this.userService.getUsers(req.body.email)
        if (users.length == 0 || req.session == null) {
            res.json({ result: false })
        } else {
            let user = users[0];
            req.session["user"] = user.id
            let valid = await this.userService.checkPassword(req.body.password, user.password);
            res.json({ result: valid })
        }
    }

}