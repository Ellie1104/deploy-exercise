
import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
await knex.schema.dropTableIfExists('users')
   await knex.schema.createTable('users',(table)=>{
        table.increments()
        table.string('username')
        table.string('password')
        table.timestamps(false, true)
     
    })
    await knex.schema.dropTableIfExists('memos')
    await knex.schema.createTable('memos',(table)=>{
        table.increments()
        table.json('content')
        table.string('image_path')
        table.timestamps(false, true)
     
    })


}
// insert into users (username,password) values ('abc','ellie');

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('users')
    await knex.schema.dropTable('memos')
}

//ask what happen after down

//yarn knex migrate: xxxxx;
//up: knex.schema.alterTable
//table.string('xx')     //create
//table.string('xx').nullable().alter()     //already exist, amend to nullable
//table.renameColumn('oldname','nename')

//down knex.schema.alterTable
//table.dropColumn('xx)
//table.string('xx').notNullable().alter();
//Command: yarn knex migrate:latest


//yarn knex migrate:rollback if not commited;
//table.integer('user_id').unsigned()   //no +/-
//table.foreign('user_id').references('user.id')
