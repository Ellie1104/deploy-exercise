import Knex from 'knex';
const knexfile = require('./knexfile');
const knex = Knex(knexfile["testing"]);

describe("database",()=>{

    beforeAll( async()=>{
            await knex.migrate.rollback();
            await knex.migrate.latest();
            await knex.seed.run();
        }  
    )
    
    it("should get all users",async ( )=>{
            const users = await knex.select("*").from("users")
            expect(users.length).toBe(3);
        });

    afterAll(()=>{
        knex.destroy()
    })

})