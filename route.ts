
import express from 'express';
import {memoController} from './main'
import {upload} from './main'
import {isLoggedIn} from "./login"
export const routes = express.Router();



routes.post('/add-memo', upload.array('photo'), memoController.addMemo)


routes.get("/memos", memoController.getMemo)


routes.put('/memos/:id', isLoggedIn, memoController.editMemo)

routes.delete("/memos/:id", isLoggedIn, memoController.deleteMemo)