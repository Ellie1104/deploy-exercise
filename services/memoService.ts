import knex from 'knex';


export class MemoService {
    constructor(private knex: knex) {

    }
    public addMemo = async (content: JSON) => {
console.log(content)
        await this.knex.insert({
            content:content,
            created_at:new Date(),
            updated_at:new Date()
        }).into('memos').returning('id');
     
    }
    public getMemos = async () => {
        return this.knex.select().from("memos");
    }
    public editMemo = async (id: number, content: JSON) => {
        await this.knex("memos").update(
            {
                content:content,
                update_at:new Date()
            }
        ).where("id",id)

    }

    public deleteMemo = async (id: number) => {
        await this.knex("memos").delete("memos").where("id",id)
        return;
    }

}