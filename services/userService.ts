import Knex from 'knex'
import bcrypt from 'bcryptjs'


export class UserService {
    constructor(private knex: Knex) { }
    public getUsers = async (email: string, id = null) => {
        return (await this.knex.select('email').from('users').where('email', email))
    }

    public checkPassword = async (plainPassword: string, hashPassword: string) => {
        const match = await bcrypt.compare(plainPassword, hashPassword);
        return match;
    }

    public hashPassword = async (plainPassword: string) => {
        const SALT_ROUNDS = 10;
        const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
        return hash;
    };
    
    public insertUser = async (email: string, hash: string) => {
        return await this.knex.insert(
            { email: email, password: hash }
        ).into("users")

    }


    public addUser = async (email: string, password: string) => {
// let query = "TRUNCATE TABLE users;"
        // await client.query(query);
        let hash = await this.hashPassword(password);
        await this.insertUser(email, hash);
    }

}
