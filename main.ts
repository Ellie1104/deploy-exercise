//#region Import
import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import multer from 'multer'
import expressSession from 'express-session';
import { Server as SocketIO } from 'socket.io';
import http from 'http';
// import { client } from './db_connect'

import Knex from 'knex';
import { MemoService } from './services/memoService';
import { MemoController } from './controllers/memoController';
import {UserService} from './services/userService'
import{UserController} from './controllers/userController';
// import {client } from './db_connect';

//#endregion

//#region Initialize sessions multer
const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);

const knexConfig = require('./knexfile');

export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({ storage })

const sessionMiddleware = expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
});

app.use(sessionMiddleware);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
io.use((socket, next) => {
    const request = socket.request as express.Request;
    sessionMiddleware(request, request.res as express.Response, next as express.NextFunction);
});

//#endregion

//#region Memos


const userService = new UserService(knex)
export const userController = new UserController(userService)
import { loginRoutes } from './login'
app.use(loginRoutes)
//#endregion

const memoService = new MemoService(knex);
export const memoController = new MemoController(memoService,io)
import {routes} from "./route"
app.use(routes)


//#region Default
app.get("/", (req, res, next) => {
    res.sendFile(path.resolve("./public/index.html"))
});
app.get("/zeus", (req, res, next) => {
    res.sendFile(path.resolve("./public/zeus.html"))
});



app.use(express.static(path.resolve('./public')));
app.use('/uploads', express.static(path.resolve('/uploads')))
app.use((req, res, next) => {
    res.sendFile(path.resolve("./public/404.html"))
});
//#endregion

const PORT = process.env.PORT;
// client.connect().then(() => {
//     console.log(process.env.DB_NAME, process.env.DB_USERNAME)
   
// });



server.listen(PORT, () => {
    console.log(`Listening at ${PORT}`);
});